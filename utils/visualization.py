import matplotlib

matplotlib.use("Agg")
import matplotlib.pyplot as plt


def plot_learning_curve(train_error, val_error, output_path):
    x = range(train_error.shape[0])
    plt.plot(x, val_error, label="val error", color="green")
    plt.plot(x, train_error, label="train error", color="blue")
    plt.xlim((0, len(x)))
    plt.ylim((0, 1))
    plt.title("Learning curves")
    plt.legend()
    plt.savefig(output_path)
    plt.close()
