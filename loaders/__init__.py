from abc import ABCMeta, abstractmethod
from random import shuffle
from math import ceil
import numpy as np
import theano


class Dataset(object):
    __class__ = ABCMeta

    @abstractmethod
    def __getitem__(self, index):
        raise NotImplementedError

    @abstractmethod
    def __len__(self):
        raise NotImplementedError


class DataLoader(object):
    def __init__(self, dataset, batch_size, shuffle=True, transformer=lambda x: x):
        self.transformer = transformer
        self.batch_size = batch_size
        self.dataset = dataset
        self.shuffle = shuffle

    def generate_mini_batches(self):
        indexes = range(len(self.dataset))
        if self.shuffle:
            shuffle(indexes)

        for i in xrange(0, len(self.dataset), self.batch_size):
            batch_indexes = indexes[i:i + self.batch_size]
            items = [self.dataset[j] for j in batch_indexes]
            images, targets = zip(*items)
            images = np.array(images, dtype=theano.config.floatX)
            yield self.transformer(images), targets

    def __len__(self):
        return ceil(float(len(self.dataset)) / self.batch_size)
