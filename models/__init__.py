from lasagne.layers import get_output, get_all_params, get_all_param_values, set_all_param_values, get_output_shape
from utils.file_system import pickle, unpickle


class Model(object):
    def get_output(self, input_var, deterministic=False):
        return get_output(self.net["output"], input_var, deterministic=deterministic)

    def get_parameters(self, trainable=True):
        return get_all_params(self.net["output"], trainable=trainable)

    def save_params(self, path):
        params = get_all_param_values(self.net["output"])
        pickle(params, path)

    def load_params(self, path):
        params = unpickle(path)
        set_all_param_values(self.net["output"], params)

    def __str__(self):
        return self.__class__.__name__

    def print_layers_shapes(self):
        for layer_name, layer in self.net.iteritems():
            layer_shape = get_output_shape(layer)
            print "{layer_name}: {layer_shape}".format(layer_name=layer_name, layer_shape=layer_shape)
