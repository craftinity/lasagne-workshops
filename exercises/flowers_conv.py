"""
Usage: flowers_conv.py   [options]

Options:
--imagenet-model-path VGG16_IMAGENET_PRETRAINED
--epochs EPOCHS                 [default: 10]
--batch-size BATCH_SIZE         [default: 4]
--lr LEARNING_RATE              [default: 1.e-3]
"""
import os
import os.path as op
from random import randint

import numpy as np
import theano
from docopt import docopt

from loaders.flowers import Flowers
from models.vgg16 import FlowersVgg16
from training.trainers import NesterovTrainer
from utils.file_system import create_dir_if_does_not_exist
from utils.visualization import plot_learning_curve

EXERCISE_NAME = "flowers_conv"


def transformer(input_batch):
    mean = np.array([103.939, 116.779, 123.68], dtype=np.float32)
    mean_extracted_batch = input_batch - mean.reshape((1, 1, 1, 3))
    return np.transpose(mean_extracted_batch, (0, 3, 1, 2))


def random_crop(img, crop_size):
    boundary = img.shape[0] - crop_size
    x_boundary = randint(0, boundary)
    y_boundary = randint(0, boundary)
    return img[y_boundary:y_boundary + crop_size, x_boundary:x_boundary + crop_size]


def augmenting_transformer(input_batch):
    input_batch = np.array([random_crop(img, 224) for img in input_batch], dtype=theano.config.floatX)
    return transformer(input_batch)


def train(epochs, batch_size, learning_rate, imagenet_model_path):
    data_root = os.getenv("DATA_ROOT")
    exercise_dir = op.join(data_root, EXERCISE_NAME)
    create_dir_if_does_not_exist(exercise_dir)

    train_dataset = Flowers("train", img_resolution=(256, 256))
    validation_dataset = Flowers("valid")
    test_dataset = Flowers("test")

    model = FlowersVgg16(imagenet_model_path)
    model_path = op.join(exercise_dir, "{model_name}.pkl".format(model_name=str(model)))

    plot_path = op.join(exercise_dir, "learning_curve_{model_name}.png".format(model_name=str(model)))

    trainer = NesterovTrainer(model, epochs, batch_size, learning_rate, 0.9, input_type="conv",
                              transformer=augmenting_transformer)
    train_acc, _, val_acc, _ = trainer.train(train_dataset, validation_dataset)

    plot_learning_curve(1. - train_acc, 1. - val_acc, plot_path)

    model.save_params(model_path)
    model.load_params(model_path)

    test_loss, test_accuracy = trainer.test(test_dataset)
    print "Test accuracy: {accuracy}, Test loss: {loss}".format(accuracy=test_accuracy, loss=test_loss)


def main():
    args = docopt(__doc__)
    epochs = int(args["--epochs"])
    batch_size = int(args["--batch-size"])
    learning_rate = float(args["--lr"])
    imagenet_model_path = args["--imagenet-model-path"] if args["--imagenet-model-path"] else None
    train(epochs, batch_size, learning_rate, imagenet_model_path)


if __name__ == "__main__":
    main()
