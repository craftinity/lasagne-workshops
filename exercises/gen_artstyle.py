"""
Usage: gen_artstyle.py  CONTENT_IMAGE_PATH STYLE_IMAGE_PATH VGG_PARAMS_PATH OUT_DIR [options]

Options:
--iterations-num ITERATIONS_NUM         [default: 10]
"""
from docopt import docopt
from models.artstyle import ArtImageGenerator
from utils.file_system import create_dir_if_does_not_exist


def generate(content_image_path, style_image_path, iter_num, vgg_params_path, out_dir):
    create_dir_if_does_not_exist(out_dir)
    generator = ArtImageGenerator(vgg_params_path, iter_num)
    generator.generate_image(style_image_path, content_image_path, out_dir)


def main():
    args = docopt(__doc__)
    content_image_path = args["CONTENT_IMAGE_PATH"]
    style_image_path = args["STYLE_IMAGE_PATH"]
    vgg_params_paths = args["VGG_PARAMS_PATH"]
    out_dir = args["OUT_DIR"]
    iter_num = int(args["--iterations-num"])
    generate(content_image_path, style_image_path, iter_num, vgg_params_paths, out_dir)


if __name__ == '__main__':
    main()
